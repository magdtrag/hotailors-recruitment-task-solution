import { inject, injectable } from "inversify";
import { IFunctionService } from "./IFunctionService";
import { COMMON_TYPES } from "../../ioc/commonTypes";
import { ILogger } from "../../commonServices/iLogger";
import { PokemonFetcher } from "./PokemonFetcher";

@injectable()
export class FunctionService implements IFunctionService<any> {

    @inject(COMMON_TYPES.ILogger)
    private readonly _logger: ILogger;

    public async processMessageAsync(msg: any, idList: number[], type: string): Promise<any> {
        const fetcher: PokemonFetcher = new PokemonFetcher();
        const pokemonsArray: any = await fetcher.getPokemons(type, idList);
        this._logger.verbose(`${JSON.stringify(msg)}`);
        return {pokemons: pokemonsArray};
    }
}
