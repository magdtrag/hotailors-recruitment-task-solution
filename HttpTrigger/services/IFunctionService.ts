export interface IFunctionService<T> {
    processMessageAsync(message: T, idList: number[], type: string): Promise<any>;
}
