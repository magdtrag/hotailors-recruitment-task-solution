import axios from "axios";
import _ from "lodash";

interface IPokemonTypes {
    types: IPomidor[];
    name: string;
}

interface IPomidor {
    slot: number;
    type: IPokemonType;
}

interface IPokemonType {
    name: string;
    url: string;
}

export class PokemonFetcher {

    private static readonly _BASE_POKEAPI_URL: string = "https://pokeapi.co/api/v2/pokemon/";
    private readonly _pokemonList: string[];
    private _desiredPokemonType: string;
    private _listOfPokemonId: number[];

    public constructor() {
        this._pokemonList = [];
    }

    public async getPokemons(type: string, idList: number[]): Promise<string[]> { 
        this._desiredPokemonType = type;
        this._listOfPokemonId = idList;
        await this.fillPokemonsList();
        return this._pokemonList;
    }

    private async fillPokemonsList(): Promise<void> {
        for (const pokemonId of this._listOfPokemonId) {
            const pokemonInfoEndpoint: string = PokemonFetcher._BASE_POKEAPI_URL + pokemonId;
            const pokemonInfo: any = await axios.get(pokemonInfoEndpoint);
            this.addPokemonIfMeetsExpectations(pokemonInfo.data);
        }
    }

    private addPokemonIfMeetsExpectations(pokemonInfo: IPokemonTypes): void {
        if (this.isPokemonTypeCompliant(pokemonInfo.types)) {
            this._pokemonList.push(pokemonInfo.name);
        }
    }

    private isPokemonTypeCompliant(listOfPokemonTypes: IPomidor[]): boolean {
        return listOfPokemonTypes.some((type) => type.type.name === this._desiredPokemonType);
    }
}
